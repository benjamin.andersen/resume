import React from 'react'

function Header() {
    return (
        <div id='header'>
            <h1>Ben Andersen</h1>

            <h4>
                <ul className='header contact'>
                    <li>
                        <a href='tel:(513) 262-9918'>(513) 262-9918</a>
                    </li>

                    <h5>&ensp;|&ensp;</h5>

                    <li>
                        <a href='mailto:ander2b8@mail.uc.edu'>ander2b8@mail.uc.edu</a>
                    </li>

                    <h5>&ensp;|&ensp;</h5>

                    <li>
                        <a href='https://gitlab.com/benjamin.m.andersen'>GitLab</a>
                    </li>
                </ul>
            </h4>
        </div>
    )
}

export default Header