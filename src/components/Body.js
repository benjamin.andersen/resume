import React from 'react'

// row component template
import Row from './Row'

// import data
import objective from '../data/rowData/objective'
import technicalSkills from '../data/rowData/technicalSkills'
import experience from '../data/rowData/experience'
import education from '../data/rowData/education'
import references from '../data/rowData/references'

function Body() {
    return (
        <div id = 'body'>
            <Row row = {objective}/><hr />
            <Row row = {technicalSkills}/><hr />
            <Row row = {experience}/><hr />
            <Row row = {education}/><hr />
            <Row row = {references}/>
        </div>
    )
}

export default Body