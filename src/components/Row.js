import React from 'react'

// row component template
function Row(props) {
    if (!props.row.className) {
        props.row.className = 'row'
    } else {
        props.row.className += ' row'
    }

    return (
        <div className = {props.row.className}>
            <div className = 'col first'>
                <div className = 'row container'>
                    {props.row.title}
                </div>
            </div>

            {props.row.content}
        </div>
    )
}

export default Row