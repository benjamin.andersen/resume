import React from 'react'

// card component template
function Card(props) {
    if (!props.card.className) {
        props.card.className = 'sub-col'
    } else {
        props.card.className += ' sub-col'
    }

    return (
        <div className = {props.card.className}>
            <div className = 'sub-row title'>
                <h3>{props.card.title}</h3>
            </div>

            <div className = 'sub-row content'>
                {props.card.content}
            </div>
        </div>
    )
}

export default Card