import React from 'react'

// main components
import Header from './components/Header'
import Body from './components/Body'

function App() {
    return (
        <div id = 'inner'>
            <Header /><hr />
            <Body />
        </div>
    )
}

export default App