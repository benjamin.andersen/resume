import React from 'react'

// card component template
import Card from '../../components/Card'

// import data
import nrg from '../cardData/experience/nrg'
import magnolia from '../cardData/experience/magnolia'
import projects from '../cardData/experience/projects'
import affiliations from '../cardData/experience/affiliations'

const experience = {
    title: (
        <h2>Experience</h2>
    ),

    content: (
        <div className = 'col container'>
            <div className = 'row container one'>
                <Card card = {nrg}/>
                <Card card = {magnolia}/>
            </div>

            <hr className = 'hide'/>
            <div className = 'row container hide'>
                <Card card = {projects}/>
                <Card card = {affiliations}/>
            </div>

            <div className = 'hidden container'>
                <Card card = {nrg}/><hr />
                <Card card = {magnolia}/><hr />
                <Card card = {projects}/><hr />
                <Card card = {affiliations}/>
            </div>
        </div>
    )
}

export default experience