import React from 'react'

// card component template
import Card from '../../components/Card'

// import data
import uc from '../cardData/education/uc'
import walnut from '../cardData/education/walnut'

const education = {
    title: (
        <h2>Education</h2>
    ),

    content: (
        <div className = 'col container'>
            <div className = 'row container two'>
                <Card card = {uc}/>
                <Card card = {walnut}/>
            </div>
            
            <div className = 'hidden container'>
                <Card card = {uc}/><hr />
                <Card card = {walnut}/>
            </div>
        </div>
    )
}

export default education