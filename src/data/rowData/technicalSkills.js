import React from 'react'

const technicalSkills = {
    title: (
        <h2>Technical <br className = 'hide'/>Skills</h2>
    ),

    content: (
        <div className = 'col container'>
            <div className = 'row container'>
                <h4>
                    Python,
                    Raspberry Pi,
                    HTML/CSS,
                    Apache,
                    C++,
                    React,
                    Gatsby,
                    Node,
                    JavaScript,
                    Bash,
                    LabVIEW,
                    Matlab,
                    Visual Basic
                </h4>
            </div>
        </div>
    )
}

export default technicalSkills