import React from 'react'

const objective = {
    className: 'first',
    title: (
        <h2>Objective</h2>
    ),

    content: (
        <div className = 'col container'>
            <div className = 'row container'>
                <h4>
                    Second-year junior Computer Engineering major and
                    Computer Science minor with 63 credit hours and a 3.87 GPA.
                    Interested in gaining experience in full-stack and
                    software development from a 2022 Spring Internship,
                    and in contributing to the success of a real-world project.
                </h4>
            </div>
        </div>
    )
}

export default objective