import React from 'react'

// card component template
import Card from '../../components/Card'

// import data
import magnolia from '../cardData/references/magnolia'
import walnut from '../cardData/references/walnut'

const references = {
    title: (
        <h2>References</h2>
    ),

    content: (
        <div className = 'col container'>
            <div className = 'row container three'>
                <Card card = {magnolia}/>
                <Card card = {walnut}/>
            </div>

            <div className = 'hidden container'>
                <Card card = {magnolia}/><hr />
                <Card card = {walnut}/>
            </div>
        </div>
    )
}

export default references