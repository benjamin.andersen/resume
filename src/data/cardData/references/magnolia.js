import React from 'react'

const magnolia = {
    title: 'Magnolia International Ltd',
    content: (
        <h4>
            <ul>
                <li>
                    Supervisor&ensp;|&nbsp;
                    <a href='mailto:pete.ryland@magnolia-cms.com'>Pete Ryland</a>
                </li>

                <li>
                    Supervisor&ensp;|&nbsp;
                    <a href='mailto:pat.hills@magnolia-cms.com'>Pat Hills</a>
                </li>

                <li>
                    Human Resources&ensp;|&nbsp;
                    <a href='./mgn-recommendation.pdf'>Recommendation</a>
                </li>
            </ul>
        </h4>
    )
}

export default magnolia