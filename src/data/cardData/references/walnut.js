import React from 'react'

const walnut = {
    title: 'Walnut Hills High School',
    content: (
        <h4>
            <ul>
                <li>
                    Pre-Calculus AA&ensp;|&nbsp;
                    <a href='mailto:MassieE@cps-k12.org'>Emma Massie</a>
                </li>

                <li>
                    English 10&ensp;|&nbsp;
                    <a href='mailto:MinanoC@cps-k12.org'>Christine Mi&ntilde;ano</a>
                </li>

                <li>
                    AP Calculus BC&ensp;|&nbsp;
                    <a href='mailto:GordoWi@cps-k12.org'>William Gordon</a>
                </li>

                <li>
                    AP Physics&ensp;|&nbsp;
                    <a href='mailto:HaanSan@cps-k12.org'>Sandee Coats-Haan</a>
                </li>

                <li>
                    Marching Band&ensp;|&nbsp;
                    <a href='mailto:CanterR@cps-k12.org'>Richard Canter</a>
                </li>
            </ul>
        </h4>
    )
}

export default walnut