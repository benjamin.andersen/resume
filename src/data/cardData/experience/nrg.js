import React from 'react'

const nrg = {
    title: 'NRG-Edge',
    content: (
        <h4>
            August 2020 - December 2020&ensp;|&ensp;Software Developer Intern<br /><br />
            Five-month remote software developer internship<br />
            <ul>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </h4>
    )
}

export default nrg