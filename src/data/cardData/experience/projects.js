import React from 'react'

const projects = {
    title: 'Projects',
    content: (
        <h4>
            <ul>
                <li>Built a responsive, self-hosted website <a href='https://elohmrow.com/resume'>resume</a> powered by React and Apache</li>
                <li>Solved several Python challenges for fun</li>
                <li>Created a rudimentary <a href='https://gitlab.com/benjamin.m.andersen/python-projects/-/tree/master/edabit/roadNavigation'>road navigation</a> script with Python</li>
                <li>Developed a <a href='https://gitlab.com/benjamin.m.andersen/python-projects/-/tree/master/projects/sudokuSolver'>sudoku puzzle solver</a> using my own algorithm</li>
                <li>Built a LEGO Mindstorm EV3 with line-following, weight determination, and color sensing software</li>
            </ul>
        </h4>
    )
}

export default projects