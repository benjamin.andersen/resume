import React from 'react'

const affiliations = {
    title: 'Affiliations',
    content: (
        <h4>
            <ul>
                <li>NERDS robotics (Spring 2020):
                    <ul className='sub-list'>
                        <li>Volunteered 25.5 hours of community service with an impact value of $586.25</li>
                        <li>Tutored reading, math, and critical thinking to grades K-8</li>
                    </ul>
                </li>

                <li>Volunteered 17 hours of community with an impact value of $390.83 (Fall 2019)</li>
                <li>Walnut Hills Marching Band (2016 - 2019):
                    <ul className='sub-list'>
                        <li>Led the front ensemble as section leader and center marimba for two years</li>
                    </ul>
                </li>

                <li><a href='http://onlineraceresults.com/event/view_event.php?event_id=18874'>Flying Pig Marathon</a>
                    &nbsp;and the <a href='https://www.kidney.org/events/kidney-walk/2017-cincinnati-kidney-walk'>Kidney Walk</a> (2017 - 2018):

                    <ul className='sub-list'>
                        <li>Played cymbals in drumline for several hours to raise money for kidney and cardiovasular disease charities</li>
                    </ul>
                </li>

                <li>Participated in the Norwood YMCA CIT Program (2014)</li>
            </ul>
        </h4>
    )
}

export default affiliations