import React from 'react'

const magnolia = {
    title: 'Magnolia International Ltd',
    content: (
        <h4>
            June 2018&ensp;|&ensp;Praktikant DevOps<br /><br />
            One-month DevOps internship in Switzerland<br />
            <ul>
                <li>Created Raspberry Pi images for wall-mounted displays in product development offices</li>
                <li>Developed a Python URL shortener service with an LDAP-enabled front-end and complete HTML/CSS</li>
                <li>Updated project documentation and presented developments</li>
            </ul>
        </h4>
    )
}

export default magnolia