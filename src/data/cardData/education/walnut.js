import React from 'react'

const walnut = {
    title: 'Walnut Hills High School',
    content: (
        <h4>
            2014 - 2019&ensp;|&ensp;Cincinnati, OH<br /><br />
            4.13 GPA&ensp;|&ensp;32 Credit Hours<br />
            <ul>
                <li>Multivariable Calculus (College Credit Plus)</li>
                <li>Passed seven AP classes</li>
                {/* AP US History (1)
                AP English Language (2)
                AP Calculus BC (5)
                AP Physics 1 (4)
                AP Physics 2 (3)
                AP Physics C Mechanics (5)
                AP Physics C Electricity and Magnetism (4) */}
                <li>Multiple band studies</li>
                <li>Honor Roll 14 times in 16 quarters</li>
                <li>Maxima Cum Laude from the NLE (2017)</li>
                <li>Section Leader in the Walnut Hills Marching Blue and Gold</li>
            </ul>
        </h4>
    )
}

export default walnut