import React from 'react'

const uc = {
    title: 'University of Cincinnati',
    content: (
        <h4>
            2019 - Present&ensp;|&ensp;Cincinnati, OH<br /><br />
            3.87 GPA&ensp;|&ensp;63 Credit Hours<br /><br />
            Bachelor of Science<br />
            <ul>
                <li>Computer Engineering Major</li>
                <li>Computer Science Minor</li>
            </ul>
        </h4>
    )
}

export default uc